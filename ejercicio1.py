proteinas={'6S1A':'Ligand binding domain of the P. putida receptor PcaY_PP',\
    '6S18':'Ligand binding domain of the P. putida receptor PcaY_PP in complex with glycerol',\
    '6S33':'Ligand binding domain of the P. putida receptor PcaY_PP in complex with Protocatechuate',\
    '6S3B':'Ligand binding domain of the P. putida receptor PcaY_PP in complex with benzoate',\
    '6S37':'Ligand binding domain of the P. putida receptor PcaY_PP in complex with salicylic acid',\
    '6S38':'Ligand binding domain of the P. putida receptor PcaY_PP in complex with quinate',\
    '6S87':'Crystal structure of 2-methylcitrate synthase (PrpC) from Pseudomonas aeruginosa in complex with oxaloacetate.',\
    '6S96':'Crystal structure of the catalytic domain of UBE2S C118A.',\
    '6S98':'Crystal structure of the catalytic domain of UBE2S WT.',\
    '6T27':'Structure of Human Aldose Reductase Mutant L301A with a Citrate Molecule Bound in the Anion Binding Pocket'}


def agregar(dicc):
    key=input('Ingrese el codigo de la proteina:\n')
    value=input('Ingrese el nombre de la proteina:\n')
    dicc[key]=value

def editar(dicc):
    print('Los elementos de la lista son:\n')
    for llave in proteinas:
      print(llave)
    print('Escriba el elemento que sea configurar:\n')
    key=input()
    value=input('Escriba el nuevo nombre de la proteina:\n')
    dicc[key]=value

def consultar(dicc):
    key=input('Ingrese el codigo que desea consultar:\n')
    print('\n El nombre de la proteina con dicho codigo es:\n')
    print(dicc.get(key,'No se encontró el valor'))

def eliminar(dicc):
    key=input('Inserte el código de la proteína que desea eliminar:\n')
    del dicc[key]
    print('Se ha eliminado correctamente la proteina')

def imprimir(dicc):
    for key , val in dicc.items() :
      print( key , val )

def programa():
    menu=int(input('Ingrese el numero de la accion que desea realizar:\n\n 1)Agregar elementos del diccionario.\n 2)Editar elementos del diccionario. \n 3)Consultar por un codigo de proteina. \n 4)Eliminar un codigo de proteina.\n 5)Imprimir el diccionario de proteinas.\n -'))
    while menu!=1 and menu!=2 and menu!=3 and menu!=4 and menu!=5:
      menu=int(input('Ingrese el numero de la accion que desea realizar:\n\n 1)Agregar elementos del diccionario.\n 2)Editar elementos del diccionario. \n 3)Consultar por un codigo de proteina. \n 4)Eliminar un codigo de proteina.\n 5)Imprimir el diccionario de proteinas.\n -'))

    if menu==1:
      agregar(proteinas)
      input('Ingrese si\n')

    if menu==2:
      editar(proteinas)

    if menu==3:
      consultar(proteinas)

    if menu==4:
      eliminar(proteinas)

    if menu==5:
      imprimir(proteinas)

    otro=input('¿Desea realizar otra accion?(Ingrese "si" si desea continuar y si desea salir del programa escriba cualquier otra cosa)\n')
    if otro=='si':
      programa()
    else:
      print('Adios,gracias por utilizar el programa.')
      return


programa()
