def buscar(L):
    global traductor
    Traducida = []
    for q in range(len(L)):
        traduccion = traductor.get(L[q], L[q])
        Traducida.append(traduccion)
    return Traducida


def mostrar(L):
    t = ''
    for i in range(len(L)):
        t = t + L[i] + ' '
    print(t)


traductor = {}


def crea_dic():
    global traductor
    print('Ingrese la palabra en español y en ingles que desea incluir en el diccionario, separadas por un ";"\n')
    nueva_palabra = input()
    if nueva_palabra.find(';')==-1:
        print('error. Por favor ingrese los caracteres correctamente')
        crea_dic()
    lista_palabra = nueva_palabra.split(';')
    p_esp = lista_palabra[0]
    p_ing = lista_palabra[1]
    traductor[p_esp] = p_ing
    continua = input('¿Desea ingresar otra palabra al diccionario (Ingrese si o no)?\n')
    while continua == 'si':
        nueva_palabra = input('Ingrese la nueva palabra al diccionario:\n')
        lista_palabra = nueva_palabra.split(';')
        p_esp = lista_palabra[0]
        p_ing = lista_palabra[1]
        traductor[p_esp] = p_ing
        continua = input('¿Desea ingresar otra palabra al diccionario (Ingrese si o no)?\n')
    while continua == 'no':
        traducir = input('¿Desea traducir una frase?("si" o "no")\n')
        while traducir == 'si':
            frase = input('Ingrese la frase que desea traducir:\n')
            palabras = frase.lower().split()
            print(palabras)
            traduccion = buscar(palabras)
            print('\n\nLa traduccion a ingles es:\n')
            mostrar(traduccion)
            traducir = input('\n¿Desea traducir otra frase?\n')
        while traducir == 'no':
            agregar_palabra = input('¿Desea agregar mas palabras al diccionario(1) o cerrar el programa(2)?\n')
            if agregar_palabra == '1':
                crea_dic()
                return
            if agregar_palabra == '2':
                print('\nAdios, gracias por utilizar el programa')
                return
            while agregar_palabra != '1' and agregar_palabra != '2':
                print('Error, ingrese una opcion valida por favor')
                agregar_palabra = '1'
    while traducir != 'si' and traducir != 'no':
        print('Error, ingrese una opcion valida por favor ')
        traducir = input('¿Desea traducir una frase?("si" o "no")\n')


crea_dic()